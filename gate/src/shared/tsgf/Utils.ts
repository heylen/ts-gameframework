

/**
 * 对象里是否有属性,通常用于判断将object{}当作键值对来使用的场景
 * @date 2022/3/31 - 下午4:45:24
 *
 * @export
 * @param {*} object
 * @returns {boolean}
 */
export function hasProperty(object: any): boolean {
    if (!object) return false;
    for (var key in object) {
        return true;
    }
    return false;
}