

export interface IApp{
    appId:string;
    appName:string;
    appSecret:string;
    addTime:Date;
}

export interface IAppLastMatchSrv{
    appId:string;
    lastMatchSrvKey:string;
    lastHeartBeatTime:Date;
}