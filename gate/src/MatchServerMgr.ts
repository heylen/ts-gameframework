
import { ClusterMgr, IClusterNodeCfg } from "./shared/tsgfServer/cluster/ClusterMgr";
import { WsServerOptions } from "tsrpc";
import { ServiceType as ClusterServiceType } from "./shared/tsgfServer/cluster/protocols/serviceProto";

/**匹配服务器信息*/
export interface IMatchServerInfo{
    /**服务器ID*/
    serverId:string;
    /**负责匹配的应用数量*/
    matchAppCount:number;
}

/**匹配服务器管理节点， 依赖redis，mysql*/
export class MatchServerMgr extends ClusterMgr<IMatchServerInfo>{
    constructor(getNodesCfg: () => IClusterNodeCfg[], serverOption: Partial<WsServerOptions<ClusterServiceType>>) {
        super("MatchServer", getNodesCfg, serverOption);

    }
}