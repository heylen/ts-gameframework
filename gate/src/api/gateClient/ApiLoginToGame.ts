import { ApiCall } from "tsrpc";
import { ReqLoginToGame, ResLoginToGame } from "../../shared/gateClient/protocols/PtlLoginToGame";
import { allotGameLoginToken } from "../../shared/tsgfServer/gfConfigMgr";
import { IUserInfo } from "../../shared/gateClient/UserInfo";
import { v4 } from "uuid";

export async function ApiLoginToGame(call: ApiCall<ReqLoginToGame, ResLoginToGame>) {
    var userInfo: IUserInfo = {
        userId: v4(),
        userName: call.req.userName,
    };
    var gameLoginToken = await allotGameLoginToken(call.req.gameServerId, userInfo);
    call.succ({
        gameLoginToken: gameLoginToken
    });
}