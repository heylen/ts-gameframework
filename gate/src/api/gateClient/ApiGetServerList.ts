import { ApiCall } from "tsrpc";
import { ReqGetServerList, ResGetServerList } from "../../shared/gateClient/protocols/PtlGetServerList";
import { GameServerMgr } from "../../GameServerMgr";


export async function ApiGetServerList(call: ApiCall<ReqGetServerList, ResGetServerList>) {
    var list = await GameServerMgr.getServersFromRedis();
    call.succ({
        GameServerList: list.map(n => n.nodeInfo)
    })
}