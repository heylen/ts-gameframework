# Summary

Date : 2022-04-04 22:08:10

Directory d:\Dev\JS\ts-gameframework\

Total : 31 files,  10293 codes, 164 comments, 98 blanks, all 10555 lines

summary / [details](details.md) / [diff summary](diff.md) / [diff details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 3 | 9,504 | 0 | 1 | 9,505 |
| TypeScript | 23 | 736 | 157 | 89 | 982 |
| JSON with Comments | 1 | 19 | 0 | 0 | 19 |
| Docker | 1 | 15 | 7 | 8 | 30 |
| JavaScript | 1 | 10 | 0 | 0 | 10 |
| Batch | 2 | 9 | 0 | 0 | 9 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 31 | 10,293 | 164 | 98 | 10,555 |
| gate | 31 | 10,293 | 164 | 98 | 10,555 |
| gate\deploy | 2 | 9 | 0 | 0 | 9 |
| gate\src | 21 | 660 | 141 | 80 | 881 |
| gate\src\api | 3 | 36 | 0 | 6 | 42 |
| gate\src\api\gameCluster | 1 | 13 | 0 | 3 | 16 |
| gate\src\api\gateClient | 2 | 23 | 0 | 3 | 26 |
| gate\src\shared | 15 | 522 | 137 | 59 | 718 |
| gate\src\shared\gameCluster | 5 | 255 | 80 | 25 | 360 |
| gate\src\shared\gameCluster\protocols | 3 | 119 | 8 | 7 | 134 |
| gate\src\shared\gateClient | 6 | 197 | 38 | 26 | 261 |
| gate\src\shared\gateClient\protocols | 3 | 139 | 10 | 13 | 162 |
| gate\src\shared\tsgf | 4 | 70 | 19 | 8 | 97 |
| gate\test | 1 | 16 | 10 | 8 | 34 |
| gate\test\api | 1 | 16 | 10 | 8 | 34 |

summary / [details](details.md) / [diff summary](diff.md) / [diff details](diff-details.md)