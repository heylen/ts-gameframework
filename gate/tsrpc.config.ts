import { CodeTemplate, TsrpcConfig } from 'tsrpc-cli';

const tsrpcConf: TsrpcConfig = {
    // Generate ServiceProto
    proto: [
        {
            ptlDir: 'src/shared/gateClient/protocols', // Protocol dir
            output: 'src/shared/gateClient/protocols/serviceProto.ts', // Path for generated ServiceProto
            apiDir: 'src/api/gateClient',   // API dir
            docDir: 'docs',     // API documents dir
            ptlTemplate: CodeTemplate.getExtendedPtl(),
            // msgTemplate: CodeTemplate.getExtendedMsg(),
        },
        {
            ptlDir: 'src/shared/tsgfServer/cluster/protocols',
            output: 'src/shared/tsgfServer/cluster/protocols/serviceProto.ts',
        }
    ],
    // Sync shared code
    sync: [
        {
            from: 'src/shared/tsgf',
            to: '../backend/src/shared/tsgf',
            type: 'copy'     // Change this to 'copy' if your environment not support symlink
        },
        {
            from: 'src/shared/tsgf',
            to: '../frontend/assets/shared/tsgf',
            type: 'copy'     // Change this to 'copy' if your environment not support symlink
        },
        {
            from: 'src/shared/tsgfServer',
            to: '../backend/src/shared/tsgfServer',
            type: 'copy'     // Change this to 'copy' if your environment not support symlink
        },
        {
            from: 'src/shared/gateClient',
            to: '../frontend/assets/shared/gateClient',
            type: 'copy'     // Change this to 'copy' if your environment not support symlink
        },
        {
            from: 'src/shared/gateClient',
            to: '../backend/src/shared/gateClient',
            type: 'copy'     // Change this to 'copy' if your environment not support symlink
        }
    ],
    // Dev server
    dev: {
        autoProto: true,        // Auto regenerate proto
        autoSync: true,         // Auto sync when file changed
        autoApi: true,          // Auto create API when ServiceProto updated
        watch: 'src',           // Restart dev server when these files changed
        entry: 'src/index.ts',  // Dev server command: node -r ts-node/register {entry}
    },
    // Build config
    build: {
        autoProto: true,        // Auto generate proto before build
        autoSync: true,         // Auto sync before build
        autoApi: true,          // Auto generate API before build
        outDir: 'dist',         // Clean this dir before build
    }
}
export default tsrpcConf;