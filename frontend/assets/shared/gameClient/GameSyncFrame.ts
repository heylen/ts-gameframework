import { MsgInpFrame } from "./protocols/MsgInpFrame";

/**连接输入的操作内容*/
export interface ConnectionInputOperate {
    [key: string]: any;
}

/**连接输入帧(包含多个操作)*/
export interface ConnectionInputFrame {
    /** 来源的连接ID */
    connectionId: string;
    /**本帧本用户的操作列表*/
    operates: ConnectionInputOperate[];
    /**可自行拓展其他字段*/
    [key: string]: any;
}

/**游戏同步帧*/
export interface GameSyncFrame {
    /** 连接输入帧列表, undefined|null 则表示空帧 */
    connectionInputs: ConnectionInputFrame[];

    /**可自行拓展其他字段*/
    [key: string]: any;
}

/**
 * 构建游戏同步帧时走这里
 * @param game 
 * @returns game sync frame 
 */
export function buildGameSyncFrame():GameSyncFrame{
    return {
        connectionInputs:[],
    };
}
/**
 * 构建连接输入帧
 * @param game 
 * @param conn 
 * @param inpFrame 
 * @returns connection input frame 
 */
export function buildConnectionInputFrame(connectionId:string, inpFrame:MsgInpFrame):ConnectionInputFrame{
    return {
        connectionId: connectionId,
        operates: inpFrame.operates,
    };
}