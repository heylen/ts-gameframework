
/**玩家*/
export interface IPlayer {
    connId: string;
    userName: string;
    /**国家ID,一个玩家一个国家,一一对应*/
    countryId: string;
}

/**国家*/
export interface ICountry {
    /**国家ID*/
    countryId: string;
    /**所属玩家连接ID, null则为电脑*/
    playerConnId: string | null;
    /**国家名称, 如果是玩家则默认使用玩家名作为国家名, 如果是中立则服务器赋予随机国家名*/
    name: string;
}

/**区域*/
export interface IArea {
    /**区域索引*/
    areaIndex: number;
    /**所属国家ID. null表示没有所属国家 */
    countryId: string | null;
    /**兵力上限*/
    troopsMax: number;
    /**当前兵力*/
    troopsCurr: number;
    /**当前兵力计算值(临时存储计算中间值, 实际值将赋给troopsCurr字段)*/
    troopsCurrCalculate: number;
    /**兵力增长值(每秒涨多少)*/
    troopsInc: number;
    /**上次增兵的帧索引*/
    troopsPreFSIndex: number;
    /**坐标*/
    x: number;
    z: number;
}

/**士兵*/
export interface ITroop {
    /**士兵唯一标识*/
    troopId: string;
    /**所属国家ID*/
    countryId: string;
    /**来源区域索引*/
    fromAreaIndex: number;
    /**目标区域索引*/
    toAreaIndex: number;
    /**包含兵力数*/
    troopsCount: number;
    /**坐标*/
    x: number;
    z: number;
}

export interface IMap {
    allPlayer: { [connId: string]: IPlayer };
    allCountry: { [countryId: string]: ICountry };
    allArea: IArea[];
    allAttackingTroop: { [troopId: string]: ITroop };
}


/**
 * 连接输入帧类型
 * @date 2022/2/16 - 下午4:18:09
 *
 * @export
 * @enum {number}
 */
export enum InputType {
    NewPlayer = "NewPlayer",
    RemovePlayer = "RemovePlayer",
    Attack = "Attack",
}