
import { GameSyncFrame } from "../GameSyncFrame";

/**追帧消息*/
export interface MsgAfterFrames {
    /**状态同步的数据*/
    stateData: any;
    /**状态同步所在帧索引, 即追帧的索引(afterFrames)从下一帧开始*/
    stateFrameIndex: number;
    /**要同步的游戏帧数据, 索引0的帧的帧索引值 = stateFrameIndex + 1*/
    afterFrames: GameSyncFrame[];
    /**当前最大帧索引*/
    maxSyncFrameIndex:number;
    /**服务端同步帧率(每秒多少帧)*/
    serverSyncFrameRate:number;
}