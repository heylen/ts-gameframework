import {BaseRequest, BaseResponse} from "../../tsgf/PtlBase";

export interface ReqReady extends BaseRequest {
    
}

export interface ResReady extends BaseResponse {
    
}