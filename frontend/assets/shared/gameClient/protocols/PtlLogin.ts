
/**需要连接后立即发出请求,否则超时将被断开连接 */
export interface ReqLogin {
    /**登录令牌,从gate服务器请求获得的gameLoginToken */
    loginToken:string;
}

export interface ResLogin {
    /**连接ID, 可用于断线重连*/
    connectionId:string;
}