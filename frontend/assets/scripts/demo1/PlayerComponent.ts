
import { _decorator, Component, Node, Prefab, instantiate, Vec3 } from 'cc';
import { PlayerData } from './PlayerData';
const { ccclass, property } = _decorator;



@ccclass('PlayerComponent')
export class PlayerComponent extends Component {
    public data?: PlayerData;
    start() {
    }
    update() {
        if (this.data) {
            //这里可以插值实现平滑移动,demo简单的瞬移过去把,反正仅作显示, 数据处理帧会做好判断
            if (this.node.position.x != this.data.x
                || this.node.position.z != this.data.z) {
                    //逻辑坐标映射成creator里的坐标,简单除10好了
                this.node.setPosition(this.data.x/30, this.node.position.y, this.data.z/30);
            }
        }
    }

    public init(data: PlayerData) {
        this.data = data;
    }
}