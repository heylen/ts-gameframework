import { FrameSyncExecutor } from "../../common/FrameSyncExecutor";
import { OTWAreaData } from "./OTWAreaData";
import { IArea, IMap, InputType, IPlayer, ITroop } from "../../../shared/gameClient/games/OccupationTheWarModels";
import { OTWTroopData } from "./OTWTroopData";


export interface OTWGameData {
    map: IMap | null;
    allAreaData: OTWAreaData[];
    allAttackingTroopData: { [troopId: string]: OTWTroopData };

    myPlayer: IPlayer | null;
    myCountryId: string | null;
}