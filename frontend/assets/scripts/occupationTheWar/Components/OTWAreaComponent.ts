
import { _decorator, Component, Node, Prefab, instantiate, Camera, Collider } from 'cc';
import { IArea, ICountry, IMap, IPlayer } from '../../../shared/gameClient/games/OccupationTheWarModels';
import { PHY_GROUP } from '../Const';
import { OTWAreaFlagComponent } from './OTWAreaFlagComponent';
import { OTWAreaData } from '../Data/OTWAreaData';
import { OTWGameData } from '../Data/OTWGameData';
import { OTWObjectComponent } from './OTWObjectComponent';
import { OTWFlagComponent } from './OTWFlagComponent';
const { ccclass, property } = _decorator;

@ccclass('OTWAreaComponent')
export class OTWAreaComponent extends OTWObjectComponent {
    objType = () => "Area";
    areaData: OTWAreaData | null = null;

    @property({type:Collider,tooltip:"用来游戏对象碰撞检测"})
    ObjectCollider!:Collider;
    @property({type:Collider,tooltip:"用户输入选中检测碰撞"})
    SelectCollider!:Collider;

    start() {
        this.ObjectCollider?.setGroup(PHY_GROUP.Area);
        this.ObjectCollider?.setMask(0xffffffff);
        this.SelectCollider?.setGroup(PHY_GROUP.Select);
        this.SelectCollider?.setMask(0);
    }

    onDestroy() {
        this.areaData = null;
    }

    public updateData(dt: number) {
        if (!this.areaData) return;
        if (this.areaData.area.troopsCurr < this.areaData.area.troopsMax) {
            //增兵
            this.areaData.area.troopsCurrCalculate += this.areaData.area.troopsInc * dt;
            if (this.areaData.area.troopsCurrCalculate > this.areaData.area.troopsMax) {
                this.areaData.area.troopsCurrCalculate = this.areaData.area.troopsMax;
            }
            var curr = Math.floor(this.areaData.area.troopsCurrCalculate);
            if (curr > this.areaData.area.troopsCurr) {
                this.areaData.area.troopsCurr = curr;
                this.areaData.flagComp.updateShow();
            }
        }
    }

    public static createNode(areaPrefab: Prefab, areaFlagPrefab: Prefab,
        getGameData: () => OTWGameData, area: IArea, country: ICountry | null, mainCamera: Camera, gameMgrNode: Node,
        gameFlagUIWrapNode: Node, gameObjWrapNode: Node): OTWAreaData {


        var node = instantiate(areaPrefab);
        node.name = "Area_" + area.areaIndex;

        var flagNode = OTWFlagComponent.createFlagNode(OTWAreaFlagComponent, getGameData, areaFlagPrefab, node, mainCamera, country);
        gameFlagUIWrapNode.addChild(flagNode);
        let flagComp = flagNode.getComponent(OTWAreaFlagComponent)!;

        var areaComp = node.getComponent(OTWAreaComponent)!;
        areaComp.getGameData = getGameData;

        node.setPosition(area.x, 0, area.z);
        gameObjWrapNode.addChild(node);

        const areaInfo: OTWAreaData = {
            area: area,
            country: country,
            areaNode: node,
            areaComp: areaComp,
            flagNode: flagNode,
            flagComp: flagComp
        };
        flagComp.areaInfo = areaInfo;
        areaComp.areaData = areaInfo;

        flagComp.updateShow();

        return areaInfo;
    }
}
