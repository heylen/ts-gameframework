
import { _decorator, Component, Node, Prefab, Camera, instantiate, Collider } from 'cc';
import { OTWTroopData } from '../Data/OTWTroopData';
import { OTWGameData } from '../Data/OTWGameData';
import { OTWObjectComponent } from './OTWObjectComponent';
const { ccclass, property } = _decorator;

@ccclass('OTWTroopRenderComponent')
export class OTWTroopRenderComponent extends OTWObjectComponent {
    objType = () => "Troop";
    troopData: OTWTroopData | null = null;

    onDestroy() {
        this.troopData = null;
    }

    update() {
        if (!this.troopData) return;
        var troop = this.getGameData().map?.allAttackingTroop[this.troopData.troop.troopId];
        if (!troop) return;

        if (this.node.position.x != troop.x || this.node.position.z != troop.z) {
            //先简单设置一下,表示逻辑数据和渲染数据分离,之后再做平滑和动画队列等
            this.node.setPosition(troop.x, 0, troop.z);
        }

    }
}
