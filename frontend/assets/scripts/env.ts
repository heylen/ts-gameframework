
import { GameClient } from '../shared/gameClient/GameClient';
import { GateClient } from '../shared/gateClient/GateClient';
/// #if Miniapp
/// #code import { HttpClient, WsClient } from 'tsrpc-miniapp';
/// #else
import { HttpClient, WsClient } from 'tsrpc-browser';
/// #endif

import { sys, EditBox } from "cc"

globalThis.getGateClient = (gateServerUrl: string): GateClient => {
    return new GateClient((proto, opt) => {
        return new HttpClient(proto, opt);
    }, gateServerUrl);
};
globalThis.getGameClient = (serverUrl: string): GameClient => {
    return new GameClient((proto, opt) => {
        return new WsClient(proto, opt);
    }, serverUrl);
};

