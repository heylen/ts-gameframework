
/**事件处理器实现类*/
export class EventHandlers<FunctionType extends Function>{
    private handlers: FunctionType[] = [];
    private target: any;
    
    /**
     * 构造
     * @date 2022/3/22 - 上午10:26:16
     *
     * @constructor
     * @param {*} target 事件触发的this对象
     */
    constructor(target: any) {
        this.target = target;
    }

    
    /**
     * 添加处理器
     * @date 2022/3/22 - 上午10:26:38
     *
     * @public
     * @param {FunctionType} handler
     */
    public addHandler(handler: FunctionType) {
        this.handlers.push(handler);
    }
    
    /**
     * 移出处理器
     * @date 2022/3/22 - 上午10:26:46
     *
     * @public
     * @param {FunctionType} handler
     */
    public removeHandler(handler: FunctionType) {
        for (let i = 0; i < this.handlers.length; i++) {
            if (this.handlers[i] == handler) {
                this.handlers.splice(i, 1);
                return;
            }
        }
    }

    
    /**
     * 触发所有事件
     * @date 2022/3/22 - 上午10:26:51
     *
     * @public
     * @param {...any[]} args
     */
    public trigger(...args: any[]) {
        for (let i = 0; i < this.handlers.length; i++) {
            this.handlers[i]?.apply(this.target, args);
        }
    }
}