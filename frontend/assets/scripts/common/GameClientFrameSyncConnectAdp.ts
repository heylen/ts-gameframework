
import { GameClient } from "../../shared/gameClient/GameClient";
import { MsgAfterFrames } from "../../shared/gameClient/protocols/MsgAfterFrames";
import { MsgInpFrame } from "../../shared/gameClient/protocols/MsgInpFrame";
import { MsgRequireSyncState } from "../../shared/gameClient/protocols/MsgRequireSyncState";
import { MsgSyncFrame } from "../../shared/gameClient/protocols/MsgSyncFrame";
import { MsgSyncState } from "../../shared/gameClient/protocols/MsgSyncState";
import { IFrameSyncConnect } from "./FrameSyncExecutor";

export class GameClientFrameSyncConnectAdp implements IFrameSyncConnect {

    private gameClient: GameClient;

    onAfterFrames: (msg: MsgAfterFrames) => void = (msg) => { };
    onSyncFrame: (msg: MsgSyncFrame) => void = (msg) => { };
    onRequireSyncState: (msg: MsgRequireSyncState) => void = (msg) => { };

    sendSyncState(msg: MsgSyncState): void {
        this.gameClient.client?.sendMsg('SyncState', msg);
    }
    sendInpFrame(msg: MsgInpFrame): void {
        this.gameClient.client?.sendMsg('InpFrame', msg);
    }
    disconnect(): void {
        this.gameClient.disconnect();
    }

    constructor(gameClient: GameClient) {
        this.gameClient = gameClient;
        this.gameClient.client.listenMsg("AfterFrames", msg => {
            this.onAfterFrames(msg);
        });
        this.gameClient.client.listenMsg("SyncFrame", msg => {
            this.onSyncFrame(msg);
        });
        this.gameClient.client.listenMsg("RequireSyncState", msg => {
            this.onRequireSyncState(msg);
        });
    }

}