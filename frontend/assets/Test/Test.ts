
import { _decorator, Component, Node, Prefab, Camera, instantiate, Vec3 } from 'cc';
import { OTWFlagComponent } from '../scripts/occupationTheWar/Components/OTWFlagComponent';
import { OTWTroopComponent } from '../scripts/occupationTheWar/Components/OTWTroopComponent';
import { OTWTroopFlagComponent } from '../scripts/occupationTheWar/Components/OTWTroopFlagComponent';
import { OTWTroopRenderComponent } from '../scripts/occupationTheWar/Components/OTWTroopRenderComponent';
import { OTWAreaData } from '../scripts/occupationTheWar/Data/OTWAreaData';
import { OTWGameData } from '../scripts/occupationTheWar/Data/OTWGameData';
import { OTWTroopData } from '../scripts/occupationTheWar/Data/OTWTroopData';
import { IArea, ICountry, ITroop } from '../shared/gameClient/games/OccupationTheWarModels';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = Test
 * DateTime = Sat Mar 19 2022 15:38:05 GMT+0800 (中国标准时间)
 * Author = fengssy
 * FileBasename = Test.ts
 * FileBasenameNoExtension = Test
 * URL = db://assets/Test/Test.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */

@ccclass('Test')
export class Test extends Component {
    @property({ type: Camera, tooltip: "主摄像机" })
    public MainCamera!: Camera;

    @property({ type: Prefab, tooltip: "士兵渲染模型预制体" })
    public TroopRenderPrefab?: Prefab;
    @property({ type: Prefab, tooltip: "士兵碰撞体预制体" })
    public TroopColliderPrefab?: Prefab;
    @property({ type: Prefab, tooltip: "士兵旗帜UI的预制体" })
    public TroopFlagPrefab?: Prefab;


    @property({ type: Node, tooltip: "存放所有游戏对象的3D节点" })
    public GameObjContainer?: Node;
    @property({ type: Node, tooltip: "存放所有Flag的UI节点" })
    public GameObjFlagUI?: Node;

    private gameData: OTWGameData = {
        map: {
            allCountry: {
                "1": {
                    countryId: "1",
                    playerConnId: null,
                    name: "中立1",
                }
            },
            allArea: [
                {
                    areaIndex: 0,
                    countryId: "1",
                    troopsMax: 50,
                    troopsCurr: 50,
                    troopsCurrCalculate: 50,
                    troopsInc: 1,
                    troopsPreFSIndex: 0,
                    x: 0,
                    z: 0,
                },
                {
                    areaIndex: 1,
                    countryId: "1",
                    troopsMax: 50,
                    troopsCurr: 50,
                    troopsCurrCalculate: 50,
                    troopsInc: 1,
                    troopsPreFSIndex: 0,
                    x: 3,
                    z: 0,
                }
            ],
            allPlayer: {},
            allAttackingTroop: {}
        },
        allAreaData: [],
        allAttackingTroopData: {},
        myPlayer: null,
        myCountryId: null,
    };
    private getGameData = () => this.gameData;

    private testTroopData?: OTWTroopData;

    start() {
    }

    click() {

        if (this.testTroopData) {
            this.testTroopData.renderNode.parent?.removeChild(this.testTroopData.renderNode);
            this.testTroopData.flagNode.parent?.removeChild(this.testTroopData.flagNode);
        }

        var country = this.gameData.map!.allCountry["1"];
        var troop: ITroop = {
            troopId: "1",
            countryId: country.countryId,
            fromAreaIndex: 0,
            toAreaIndex: 1,
            troopsCount: 10,
            x: 0,
            z: 0,
        };
        this.gameData.map!.allAttackingTroop[troop.troopId] = troop;


        var gameObjWrapNode = this.GameObjContainer!;
        var gameFlagUIWrapNode = this.GameObjFlagUI!;
        var mainCamera = this.MainCamera;
        var getGameData = this.getGameData;
        var troopFlagPrefab = this.TroopFlagPrefab!;


        var renderNode = instantiate(this.TroopRenderPrefab!);
        renderNode.name = "TroopRender_" + troop.troopId;

        var flagNode = OTWFlagComponent.createFlagNode(OTWTroopFlagComponent, getGameData, troopFlagPrefab, renderNode,
            mainCamera, country);
        gameFlagUIWrapNode.addChild(flagNode);
        let flagComp = flagNode.getComponent(OTWTroopFlagComponent)!;

        var renderComp = renderNode.getComponent(OTWTroopRenderComponent)!;

        renderNode.setPosition(troop.x, 0, troop.z);
        gameObjWrapNode.addChild(renderNode);

        var troopInfo: OTWTroopData = {
            troop: troop,
            fromArea: getGameData().map!.allArea[0],
            toArea: getGameData().map!.allArea[1],
            country: country,
            renderNode: renderNode,
            renderComp: renderComp,
            troopNode: renderNode,
            troopComp: new OTWTroopComponent,
            flagNode: flagNode,
            flagComp: flagComp,
        };
        renderComp.troopData = troopInfo;
        renderComp.getGameData = getGameData;
        flagComp.troopData = troopInfo;
        flagComp.getGameData = getGameData;

        flagComp.updateShow();

        this.testTroopData = troopInfo;
    }
    onTroopFlagUIUpdate(localUIPos: Vec3, distanceScale: number, customEventData: string) {
        this.testTroopData?.flagNode.setPosition(localUIPos);
        console.log('onTroopFlagUIUpdate', localUIPos, customEventData);
    }

    update(deltaTime: number) {
        if (this.testTroopData) {
            //this.testTroopData.troopComp.updateData(deltaTime);
            /*
            if(this.testTroopData.troop.x<5){
                this.testTroopData.troop.x += deltaTime;
            }
            */
            if (this.testTroopData.renderNode.position.x < 5) {
                this.testTroopData.renderNode.setPosition(this.testTroopData.renderNode.position.x + deltaTime,
                    this.testTroopData.renderNode.position.y, this.testTroopData.renderNode.position.z);
            }
        }
    }
}