
# 什么是 TSGF（ts-gameframework）

* **开源** 的联机游戏 **全栈式解决方案**：`服务端` + `客户端` 皆由 **TypeScript** 语言编写
* **MGOBE**的代替方案
* “**黑盒式**” 实现联机游戏：客户端对接SDK，不用关心通信和同步逻辑
* **自定义服务端逻辑**：拓展自己的同步逻辑和各种交互逻辑


# 结构说明

## 登陆流程

![时序图](https://fengssy.gitee.io/zgame/Games/TSGF/SequenceDiagram.png)

## 服务器结构图

![拓扑图](https://fengssy.gitee.io/zgame/Games/TSGF/ServerStructure.png)

* `gate` 是入口服务器（或者叫调度服务器）+登录服务器+集群管理服务器（简单三合一），有需要可以分离出来

* `backend` 游戏逻辑服务器，可以设计成本游戏一样，一个服务器对应一个全局游戏，所有连接玩家都视作加入这个游戏。也可以加入房间的概念

* `frontend` 游戏客户端（包含2个 demo），导入为 Creator 3.4.2 项目可运行Creator的 demo

* 使用 [TSRPC](https://tsrpc.cn/) 作为通讯框架，所以用了这个框架自带的代码生成/同步模块，导致各项目的目录名以及相对路径的固定(不熟悉乱改会导致出错)

*PS：各项目根目录需要各自执行 `npm i`，`frontend` 需要用Creator打开一次即可消除报错*



## 服务器启动、部署（gate、backend）



- ###  一键启动所有服务器（需安装 **Docker Desktop**）

    ./simpleDeploy/run.bat

（端口：7100，7101，7801 如果被占用将导致启动失败！）

- ### 开发环境调试启动各服务

* 注意！依赖：**redis**，**mysql**
* 各端目录（gate、backend）执行 `npm install` （执行过一次即可）
- `gate` 和 `backend` 目录的 `gf.*.config.json` 里的配置修改 （主要是配置 redis 和 mysql ）
* 进入 `gate` 目录，执行 `npm run dev`
* 进入 `backend` 目录，执行 `npm run dev`

- ### 手动部署各服务

* 注意！依赖：**redis**，**mysql**
* 各端目录（gate、backend）执行 `npm install` （执行过一次即可）
* 各端目录（gate、backend）执行 `npm run buildTS`，然后打包下列文件：

      ./deploy
      ./dist
      ./node_modules
      ./gf.*.config.json    (配置根据实际情况自行修改)
* windows部署，提供了快捷部署服务方式，右键管理员运行 `deploy/install_runasAdmin.cmd` 即可
* linux部署，可以使用 `pm2` 启动：`pm2 start dist/index.js`

## 启动帧同步demo（需要先启动各服务器）

- 进入 `frontend` 目录执行 `npm install` （执行过一次即可）
- 浏览简易多人混战的例子：（`frontend` 导入 Creator后，启动 `assets/occupationTheWar/occupationScene.scene`）
![对战效果](https://fengssy.gitee.io/zgame/Games/TSGF/PlayShow.gif)
*（预览模式下，要开第二个客户端则需要用 `Private` 窗口来打开预览地址，或者改用两种浏览器）*

- 浏览vue实现的例子：进入 `frontend` 目录执行 `npm run devVue` （如果没启动浏览器则手动打开输出的地址）
![示例图片](Demo1.gif)




## 二次开发、使用模块

* 暂时未提供复用性调用方式，只能拷贝源码，进行二次开发或引用模块
* 预计v1.2.0版本提供此功能

## 设计要求

- 状态数据分离，如经典的ECS设计，方便状态同步，或者帧同步中用作中间状态缓存
- 输入操作分离：方便接入帧同步

        其实只要是联机游戏，就需要做到上面说的设计，只是这里为了对接本框架而提出的设计原则

## 帧同步游戏对接要点（具体的参考 demo ）

1. 配置各端的 `gf配置文件` ，redis服务器和各服务器的ip/port
2. 设计独立的数据存储方式，如ECS
3. 游戏服务端：根据使用场景，实例化游戏对象 `Game` ，然后调用 `startGame()` (默认开启功能：帧同步+随机要求客户端同步状态给服务端)
4. 客户端：主要实现：
    - `AfterFrames`：追帧消息，客户端根据消息中的状态数据+后续帧，来复原游戏
    - `SyncFrame`：服务端帧同步消息，默认每秒60帧(即每秒60条消息)
    - `InpFrame`：客户端的输入帧消息，建议统一收集，定时发送(如30ms发送一批)，将在服务端收到的下一帧生效
    - [`RequireSyncState`]：[可选，默认开启] 服务端要求客户端将本帧的游戏数据收集上来，使用`SyncState`消息发送给服务端
    - [`SyncState`]：[可选，默认开启] 客户端将游戏数据状态化，发送给服务端，用于后续追帧时能从这个状态+后续帧来快速复原游戏
5. 入口服务器实现用户登录 （参考例子 ApiLoginToGame ）


## QQ交流群
![输入图片说明](https://images.gitee.com/uploads/images/2022/0428/180912_addf70d4_1510252.png "TSGF(ts-gamefranework)交流群群聊二维码.png")