export interface MsgAssignTask {
    /**任务唯一ID*/
    taskId:string;
    /**任务信息、数据*/
    taskInfo:any;
}
