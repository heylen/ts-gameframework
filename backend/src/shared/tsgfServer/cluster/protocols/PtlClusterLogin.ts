
export interface ReqClusterLogin {
    /**节点ID */
    nodeId: string;
    /**集群密钥 */
    clusterKey: string;
    /**节点信息 */
    nodeInfo: any;
}
export interface ResClusterLogin {
}