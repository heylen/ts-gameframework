
import { IGameServerInfo } from "../GameServer";


/**
 * 登录到游戏服务器,简单登录实现,正常要对接用户系统或者第三方授权, 是授权后服务端直接调用接口的服务端分配逻辑.然后拿到gameToken
 */
export interface ReqLoginToGame { 
    /**自定义的用户名 */
    userName:string;
    /**要登录的游戏服务器ID */
    gameServerId:string;
}

export interface ResLoginToGame {
    /**游戏服务器登录令牌,连接到游戏服务器后需要发送认证 */
    gameLoginToken:string;
}
