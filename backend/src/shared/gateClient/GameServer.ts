/**
 * 游戏服务器信息
 */
export interface IGameServerInfo {
    /**游戏服务器ID */
    serverId: string;
    /**游戏服务器名称 */
    serverName: string;
    /**游戏服务器WebSocket连接地址 */
    serverWSUrl: string;
    /**连接的客户端数量 */
    clientCount: number;
    /**拓展数据,不同的服务器不同的版本各不相同,也不用频繁升级gate服务器 */
    extendData?: any;
}

export class GameServerInfo implements IGameServerInfo{
    
    public serverId: string = '';
    public serverName: string = '';
    public serverWSUrl: string = '';
    public clientCount: number = 0;
    public extendData?: any;
}

/**游戏服务器集群节点 */
export interface GameServerCluster {
    /**游戏服务器ID */
    serverId: string;
    /**游戏服务器集群密钥,连接进集群需要校验 */
    clusterKey: string;
}