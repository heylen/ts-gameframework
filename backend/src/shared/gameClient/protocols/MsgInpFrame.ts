import { ConnectionInputOperate } from "../GameSyncFrame";

export interface MsgInpFrame {
    /**本帧本用户的操作列表*/
    operates: ConnectionInputOperate[];
    /**可自行拓展其他字段*/
    [key: string]: any;
}

// export const conf = {}