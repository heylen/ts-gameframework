
import { BaseHttpClient, BaseHttpClientOptions, BaseWsClient, BaseWsClientOptions } from "tsrpc-base-client";
import { BaseServiceType, ServiceProto } from "tsrpc-proto";

/**
 * 抽象的HTTP客户端,根据具体的环境,接入对应的客户端,让引用类型的地方不需要判断
 * @template ServiceType 
 */
export class AHttpClient<ServiceType extends BaseServiceType>{
    public client: BaseHttpClient<ServiceType>;
    constructor(buildClient: (proto: ServiceProto<ServiceType>, options?: Partial<BaseHttpClientOptions>) => BaseHttpClient<ServiceType>,
        proto: ServiceProto<ServiceType>, options?: Partial<BaseHttpClientOptions>) {
        this.client = buildClient(proto, options);
    }
}
/**
 * 抽象的Websocket客户端,根据具体的环境,接入对应的客户端,让引用类型的地方不需要判断
 * @template ServiceType 
 */
export class AWsClient<ServiceType extends BaseServiceType>{
    public client: BaseWsClient<ServiceType>;
    constructor(buildClient: (proto: ServiceProto<ServiceType>, options?: Partial<BaseWsClientOptions>) => BaseWsClient<ServiceType>,
        proto: ServiceProto<ServiceType>, options?: Partial<BaseWsClientOptions>) {
        this.client = buildClient(proto, options);
    }
}
