import { v4 } from "uuid";
import { ClientConnection } from "./server";


/**连接的集合*/
class ConnectionCollection {
    /**唯一标识*/
    public readonly id: string = v4();
    /**集合内的连接字典,连接ID=>连接对象*/
    public readonly connectionMap: Map<string, ClientConnection> = new Map<string, ClientConnection>();
    /**集合内的连接数组*/
    public readonly connections: ClientConnection[] = [];


    /**
     * 加入连接
     * @param conn 
     */
    public addConnection(conn: ClientConnection): void {
        this.removeConnection(conn.connectionId);
        this.connectionMap.set(conn.connectionId, conn);
        this.connections.push(conn);
    }
    /**
     * 移除指定连接
     * @param conn 
     */
    public removeConnection(connectionId: string): void {
        this.connectionMap.delete(connectionId);
        this.connections.remove(c => c.connectionId == connectionId);
    }

    /**清除所有连接*/
    public clearAllConnections(): void {
        this.connectionMap.clear();
        this.connections.length = 0;
    }
}


export {
    ConnectionCollection,
}