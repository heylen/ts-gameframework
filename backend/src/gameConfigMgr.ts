import path from "path";
import { GameServerInfo } from "./shared/gateClient/GameServer";
import { getConfig, startWatchConfig } from "./shared/tsgfServer/gfConfigMgr";
import { RedisConfig } from "./shared/tsgfServer/redisHelper";

var gameConfigPath = path.resolve(__dirname, '../gf.game.config.json');
export interface GfGameServer{
    /**游戏服务器ID */
    serverId: string;
    /**游戏服务器名称 */
    serverName: string;
    /**游戏服务器WebSocket连接地址 */
    serverWSUrl: string;
    /**游戏服务器连上集群的密钥 */
    clusterKey: string;
    /**拓展数据,不同的服务器不同的版本各不相同,也不用频繁升级gate服务器 */
    extendData?: any;
    /**服务侦听的端口*/
    listenPort:number;
    /**游戏集群服务连接WS地址*/
    gameClusterServerWSUrl:string;
}
export interface GfGameConfig {
    /**配置所有可以连接的集群 */
    gameServer: GfGameServer;
    /**认证模块链接的redis配置 */
    redisConfig:RedisConfig;
    
}

/**开始监控game服务器配置,并返回获取到的配置对象 */
export function startWatchGameConfig():GfGameConfig{
    var ret = startWatchConfig(gameConfigPath);
    if (!ret.succ) {
        console.error(ret.err);
    }
    return ret.data as GfGameConfig;
}

export function getGameConfig(): GfGameConfig {
    var ret = getConfig(gameConfigPath);
    if (!ret.succ) {
        console.error(ret.err);
    }
    return ret.data as GfGameConfig;
}