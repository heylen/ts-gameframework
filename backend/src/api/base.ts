import { ApiCallWs, MsgCallWs } from "tsrpc";
import { ServiceType } from "../shared/gameClient/protocols/serviceProto";

export type GameApiCall<req, res> = ApiCallWs<req, res, ServiceType>;

export type GameMsgCall<msg> = MsgCallWs<msg, ServiceType>;
