/*
一个游戏服务器,一个游戏的方式
服务启动后,调用 initAndStartGame 初始化游戏

*/
import { InputType } from "../shared/gameClient/games/DemoSimpleFrameSyncModels";
import { GameMsgCall } from "../api/base";
import { Game } from "../Game";
import { gameConnMgr, gameServer } from "../server";
import { MsgInpFrame } from "../shared/gameClient/protocols/MsgInpFrame";

/**当前游戏服务器范围的游戏实例,需要服务启动后调用 initAndStartGame(...) */
let globalGame: Game;

/**
 * 初始化和启动全局游戏
 * @param [syncFrameRate] 
 */
function initAndStartGlobalGame(syncFrameRate = 60) {
    globalGame = new Game(syncFrameRate);

    gameConnMgr.onConnAuthed((conn, reconnectOldConnId) => {
        if (globalGame.connections.length <= 0) {
            //如果这连接是第一个,则启动游戏. 放在认证通过前, 免得通过后自动发送追帧的数据不够新
            globalGame.startGame();
        }
        //发送追帧数据
        globalGame.connectionAfterFrames(conn);
        //自动插入一个新增玩家的输入
        globalGame.addConnectionInpFrame(conn.connectionId, {
            operates: [
                {
                    inputType: InputType.NewPlayer,
                    userName: conn.userInfo.userName,
                }
            ]
        });
    });
    gameConnMgr.onConnDiconnect((connId,userInfo)=>{
        if (globalGame.connections.length <= 0) {
            //如果没有连接了,则直接停止游戏
            globalGame.stopGame();
        }else{
            //玩家断开,插入一个移出玩家的输入
            globalGame.addConnectionInpFrame(connId, {
                operates: [
                    { inputType: InputType.RemovePlayer }
                ]
            });
        }
        //本游戏不做断线重连支持
        return false;
    });
}



export {
    initAndStartGlobalGame,
    globalGame,
};
