import * as path from "path";
import { startWatchGameConfig } from "./gameConfigMgr";
import { initRedisClient } from "./shared/tsgfServer/redisHelper";
import { gameServer, gameClusterClient } from "./server";

//todo: 用什么游戏demo, 可以改到配置中?
//import { initAndStartGlobalGame } from "./games/DemoSimpleFrameSyncGame";
import { initAndStartGlobalGame } from "./games/OccupationTheWarGame";

async function initServer() {
    await gameServer.autoImplementApi(path.resolve(__dirname, 'api'));
};

async function startServer() {

    await gameServer.start();
    gameServer.logger?.log("游戏服务启动成功!");

    var joinErr = await gameClusterClient.joinCluster();
    if (joinErr) {
        gameServer.logger?.log("加入集群服务器失败:" + joinErr);
    } else {
        gameServer.logger?.log("加入集群服务器成功!");
    }

    //初始化demo里的游戏
    initAndStartGlobalGame();
}

// Entry function
async function main() {
    var gateCfg = startWatchGameConfig();
    initRedisClient(gateCfg.redisConfig);

    await initServer();
    await startServer();
}
main();