
import { assert } from 'chai';
import { FrameSyncExecutor } from "../../src/FrameSyncExecutor";

describe('FrameSyncExecutor', function () {

    it('syncOneFrame 1', async function () {
        let frameSyncExecutor = new FrameSyncExecutor((msg) => {
            //console.log("onSyncOneFrame", msg);
        });

        frameSyncExecutor.onSyncOneFrameHandler();
        //运行1帧后,验证帧执行器的相关索引是否正确
        assert.ok(frameSyncExecutor.nextSyncFrameIndex === 1, "应为1,实际为" + frameSyncExecutor.nextSyncFrameIndex);
        assert.ok(frameSyncExecutor.maxSyncFrameIndex === 0, "应为0,实际为" + frameSyncExecutor.maxSyncFrameIndex);

        //运行1帧后,验证帧数组的数据是否正确
        var afterFrameArrIndex = frameSyncExecutor.getAfterFramesIndex(0);
        assert.ok(afterFrameArrIndex === 0, "应为0,实际为" + afterFrameArrIndex);

        //运行1帧后, 验证追帧信息是否正确
        var afterFramesMsg = frameSyncExecutor.buildAfterFramesMsg();
        assert.ok(afterFramesMsg.maxSyncFrameIndex === 0, "应为0,实际为" + afterFramesMsg.maxSyncFrameIndex);
        assert.ok(afterFramesMsg.stateFrameIndex === -1, "应为-1,实际为" + afterFramesMsg.stateFrameIndex);
        assert.ok(afterFramesMsg.afterFrames.length === 1, "应为1,实际为" + afterFramesMsg.afterFrames.length);

    });


    it('syncOneFrame AddInpFrame', async function () {
        //运行1次后,加入输入帧,再运行,是否输出这个输入帧
        let hasInput = false;
        let frameSyncExecutor = new FrameSyncExecutor((msg) => {
            if (hasInput) {
                //console.log("onSyncOneFrame", msg);
                assert.ok(msg.frameIndex == 1,
                    "应为1,实际为" + msg.frameIndex);
                assert.ok(msg.syncFrame.connectionInputs.length === 1,
                    "应为1,实际为" + msg.syncFrame.connectionInputs.length);
                assert.ok(msg.syncFrame.connectionInputs[0].connectionId === '1',
                    "应为1,实际为" + msg.syncFrame.connectionInputs[0].connectionId);
                assert.ok(msg.syncFrame.connectionInputs[0].operates.length === 1,
                    "应为1,实际为" + msg.syncFrame.connectionInputs[0].operates.length);
                assert.ok(msg.syncFrame.connectionInputs[0].operates[0].test === 't1',
                    "应为t1,实际为" + msg.syncFrame.connectionInputs[0].operates[0].test);
            }
        });
        frameSyncExecutor.onSyncOneFrameHandler();
        frameSyncExecutor.addConnectionInpFrame("1", {
            operates: [{ test: "t1" }]
        });
        hasInput = true;
        frameSyncExecutor.onSyncOneFrameHandler();

        //运行2帧(空帧+输入帧)后,验证帧执行器的相关索引是否正确
        assert.ok(frameSyncExecutor.nextSyncFrameIndex === 2, "应为2,实际为" + frameSyncExecutor.nextSyncFrameIndex);
        assert.ok(frameSyncExecutor.maxSyncFrameIndex === 1, "应为1,实际为" + frameSyncExecutor.maxSyncFrameIndex);

        //运行2帧(空帧+输入帧)后,验证帧数组的数据是否正确
        var afterFrameArrIndex = frameSyncExecutor.getAfterFramesIndex(0);
        assert.ok(afterFrameArrIndex === 0, "应为0,实际为" + afterFrameArrIndex);

        //运行2帧(空帧+输入帧)后, 验证追帧信息是否正确
        var afterFramesMsg = frameSyncExecutor.buildAfterFramesMsg();
        assert.ok(afterFramesMsg.maxSyncFrameIndex === 1, "应为1,实际为" + afterFramesMsg.maxSyncFrameIndex);
        assert.ok(afterFramesMsg.stateFrameIndex === -1, "应为-1,实际为" + afterFramesMsg.stateFrameIndex);
        assert.ok(afterFramesMsg.afterFrames.length === 2, "应为2,实际为" + afterFramesMsg.afterFrames.length);
    });



    it('syncOneFrame stateSync', async function () {
        //运行2次后, 同步0帧状态, 验证数据是否正确, 以及再运行1次后的数据是否正确
        let hasCheck = false;
        let frameSyncExecutor = new FrameSyncExecutor((msg) => {
            if (hasCheck) {
                //console.log("onSyncOneFrame", msg);
                assert.ok(msg.frameIndex == 2,
                    "应为2,实际为" + msg.frameIndex);
                assert.ok(msg.syncFrame.connectionInputs.length === 0,
                    "应为0,实际为" + msg.syncFrame.connectionInputs.length);
            }
        });
        frameSyncExecutor.onSyncOneFrameHandler();
        frameSyncExecutor.addConnectionInpFrame("1", {
            operates: [{ test: "t1" }]
        });
        frameSyncExecutor.onSyncOneFrameHandler();

        //同步1次帧索引为0的状态数据
        frameSyncExecutor.syncStateData({}, 0);

        //运行2帧(空帧+输入帧)再同步0帧的数据后,验证帧数组的数据是否正确
        assert.ok(frameSyncExecutor.lastStateFrameIndex === 0, "应为0,实际为" + frameSyncExecutor.lastStateFrameIndex);
        var afterFrameArrIndex = frameSyncExecutor.getAfterFramesIndex(1);
        assert.ok(afterFrameArrIndex === 0, "应为0,实际为" + afterFrameArrIndex);

        hasCheck = true;
        frameSyncExecutor.onSyncOneFrameHandler();
        hasCheck = false;
        //运行3帧后

        //验证帧执行器的相关索引是否正确
        assert.ok(frameSyncExecutor.nextSyncFrameIndex === 3, "应为3,实际为" + frameSyncExecutor.nextSyncFrameIndex);
        assert.ok(frameSyncExecutor.maxSyncFrameIndex === 2, "应为2,实际为" + frameSyncExecutor.maxSyncFrameIndex);

        //验证追帧信息是否正确
        let afterFramesMsg = frameSyncExecutor.buildAfterFramesMsg();
        assert.ok(afterFramesMsg.maxSyncFrameIndex === 2, "应为2,实际为" + afterFramesMsg.maxSyncFrameIndex);
        assert.ok(afterFramesMsg.stateFrameIndex === 0, "应为0,实际为" + afterFramesMsg.stateFrameIndex);
        assert.ok(afterFramesMsg.afterFrames.length === 2, "应为2,实际为" + afterFramesMsg.afterFrames.length);

        //再模拟执行2帧
        frameSyncExecutor.onSyncOneFrameHandler();
        frameSyncExecutor.onSyncOneFrameHandler();
        //再同步1次帧索引为1的状态数据
        frameSyncExecutor.syncStateData({}, 1);

        //验证帧执行器的相关索引是否正确
        var v = frameSyncExecutor.nextSyncFrameIndex;
        assert.ok(v === 5, "应为5,实际为" + v);
        v = frameSyncExecutor.maxSyncFrameIndex;
        assert.ok(v === 4, "应为4,实际为" + v);

        //验证追帧信息是否正确
        afterFramesMsg = frameSyncExecutor.buildAfterFramesMsg();
        assert.ok(afterFramesMsg.maxSyncFrameIndex === 4, "应为4,实际为" + afterFramesMsg.maxSyncFrameIndex);
        assert.ok(afterFramesMsg.stateFrameIndex === 1, "应为1,实际为" + afterFramesMsg.stateFrameIndex);
        assert.ok(afterFramesMsg.afterFrames.length === 3, "应为3,实际为" + afterFramesMsg.afterFrames.length);
    });
})